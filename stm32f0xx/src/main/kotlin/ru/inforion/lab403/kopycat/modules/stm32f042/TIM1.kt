package ru.inforion.lab403.kopycat.modules.stm32f042

import ru.inforion.lab403.common.logkot.logger
import ru.inforion.lab403.common.misc.MHz
import ru.inforion.lab403.common.misc.get
import ru.inforion.lab403.common.misc.hex8
import ru.inforion.lab403.common.misc.insert
import ru.inforion.lab403.kopycat.cores.base.bit
import ru.inforion.lab403.kopycat.cores.base.common.Module
import ru.inforion.lab403.kopycat.cores.base.common.ModulePorts
import ru.inforion.lab403.kopycat.cores.base.enums.Datatype.*
import ru.inforion.lab403.kopycat.cores.base.field
import java.util.logging.Level


class TIM1(parent: Module, name: String) : Module(parent, name) {
    companion object {
        private val log = logger(Level.ALL)
    }

    inner class Ports : ModulePorts(this) {
        val mem = Slave("mem")
        val irq = Master("irq")
    }

    override val ports = Ports()

    val T_CK_INT = 1.MHz
    var T_DTS: Long = 0

    private val TIM1_CR1 = object : Register(ports.mem, 0x00, WORD, "TIM1_CR1") {
        var CKD  by field(9..8)
        var ARPE by bit(7)
        var CMS  by field(6..5)
        var DIR  by bit(4)
        var OPM  by bit(3)
        var USR  by bit(2)
        var UDIS by bit(1)
        var CEN  by bit(0)

        override fun write(ea: Long, ss: Int, size: Int, value: Long) {
            super.write(ea, ss, size, value)
//            when (CKD) {
//                0b00 -> T_DTS = T_CK_INT
//                0b01 -> T_DTS = 2 * T_CK_INT
//                0b10 -> T_DTS = 4 * T_CK_INT
//                0b11 -> log.warning { "CKD -> Reserved, do not program this value" }
//            }
//
//            if (CEN == 1) {
//                log.warning { "TIM1 ENABLED!!!" }
//            }

            log.warning { "[${core.cpu.pc.hex8}] Write to $name[${ea.hex8}]: CKD = $CKD ARPE = $ARPE CMS = $CMS DIR = $DIR OPM = $OPM USR = $USR UDIS = $UDIS CEN = $CEN" }
        }

        override fun read(ea: Long, ss: Int, size: Int): Long {
            log.warning { "[${core.cpu.pc.hex8}] Read from $name[${ea.hex8}]: ${data.hex8}" }
            return super.read(ea, ss, size)
        }
    }

    private val TIM1_ARR = Register(ports.mem, 0x2C, WORD, "TIM1_ARR")

    init {
        log.warning { "TIM1 Initialization!" }
    }
}