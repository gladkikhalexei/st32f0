package ru.inforion.lab403.kopycat.modules.stm32f042

import ru.inforion.lab403.common.logkot.logger
import ru.inforion.lab403.kopycat.cores.base.common.Module
import ru.inforion.lab403.kopycat.cores.base.common.ModulePorts
import ru.inforion.lab403.kopycat.cores.base.enums.Datatype.DWORD
import ru.inforion.lab403.kopycat.modules.PIN
import java.util.logging.Level

class GPIOx(parent: Module, name: String, val index: Int) : Module(parent, name) {
    companion object { private val log = logger(Level.ALL) }
    inner class Ports : ModulePorts(this) {
        val mem = Slave("mem")
        val irq = Master("irq", PIN)
    }
    override val ports = Ports()

    private val GPIOx_MODER   = Register(ports.mem, 0x00, DWORD, "GPIO${index}_MODER",   if(index == 1) 0x2800_0000 else 0x0000_0000)
    private val GPIOx_OTYPER  = Register(ports.mem, 0x04, DWORD, "GPIO${index}_OTYPER",  0x0000_0000)
    private val GPIOx_OSPEEDR = Register(ports.mem, 0x08, DWORD, "GPIO${index}_OSPEEDR", if(index == 1) 0x0C00_0000 else 0x0000_0000)
    private val GPIOx_PUPDR   = Register(ports.mem, 0x0C, DWORD, "GPIO${index}_PUPDR",   if(index == 1) 0x2400_0000 else 0x0000_0000)
    private val GPIOx_AFRH    = Register(ports.mem, 0x24, DWORD, "GPIO${index}_AFRH",    0x0000_0000)
}