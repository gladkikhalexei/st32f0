package ru.inforion.lab403.kopycat.modules.stm32f042

import ru.inforion.lab403.kopycat.cores.base.common.Module
import ru.inforion.lab403.kopycat.cores.base.common.ModuleBuses
import ru.inforion.lab403.kopycat.modules.cortexm0.CORTEXM0
import ru.inforion.lab403.kopycat.modules.cortexm0.STK
import ru.inforion.lab403.kopycat.modules.debuggers.ARMDebugger
import java.io.File
import java.util.logging.Level

class STM32F042(parent: Module?, name: String, firmware: File) : Module(parent, name) {
    inner class Buses : ModuleBuses(this) {
        val mem = Bus("mem")
    }
    override val buses = Buses()

    private val cortex = CORTEXM0(this, "cortexm0", firmware)
    private val usart1 = USARTx(this, "usart1", 1)
    private val rcc = RCC(this, "rcc")
    private val fmi = FMI(this, "fmi")
    private val gpioa = GPIOx(this, "gpioa", 1)
    private val dbg = ARMDebugger(this, "dbg")

    init {
        STK.log.level = Level.OFF

        cortex.ports.mem.connect(buses.mem)
        usart1.ports.mem.connect(buses.mem, 0x4001_3800)
        rcc.ports.mem.connect(buses.mem, 0x4002_1000)
        fmi.ports.mem.connect(buses.mem, 0x4002_2000)
        gpioa.ports.mem.connect(buses.mem, 0x4800_0000)
        dbg.ports.breakpoint.connect(buses.mem)
        dbg.ports.reader.connect(buses.mem)
    }
}