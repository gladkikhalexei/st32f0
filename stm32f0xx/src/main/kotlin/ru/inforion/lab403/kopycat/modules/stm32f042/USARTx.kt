package ru.inforion.lab403.kopycat.modules.stm32f042

import com.fazecast.jSerialComm.SerialPort
import com.fazecast.jSerialComm.SerialPortDataListener
import com.fazecast.jSerialComm.SerialPortEvent
import ru.inforion.lab403.common.logkot.logger
import ru.inforion.lab403.common.misc.*
import ru.inforion.lab403.kopycat.cores.base.SlavePort
import ru.inforion.lab403.kopycat.cores.base.bit
import ru.inforion.lab403.kopycat.cores.base.common.Module
import ru.inforion.lab403.kopycat.cores.base.common.ModulePorts
import ru.inforion.lab403.kopycat.cores.base.enums.Datatype.DWORD
import ru.inforion.lab403.kopycat.cores.base.field
import ru.inforion.lab403.kopycat.modules.PIN
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import kotlin.concurrent.thread

/**
 * Created by user on 13.07.17.
 *
 * @param parent родительский модуль, в который будет вставлен данный
 * @param name имя инстанциированного модуля (объекта)
 * @param index номер интерфейса USART
 * @param comName путь к устройству /dev/tty
 *  Чтобы создать виртуальный порт необходимо выполнить команду в Linux:
 *   socat -d -d pty,raw,echo=0 pty,raw,echo=0
 *
 *  тогда терминал один будет создан например в /dev/ttys002, а терминал два в /dev/ttys003
 *  они будут полностью связаны, то есть запись в терминал один будет передана в терминал
 *  два и наоборот.
 * @param virtual создать виртуальный com-порт самостоятельно (только для Linux и MAC OS X)
 */
class USARTx(
        parent: Module,
        name: String,
        val index: Int,
        val comName: String? = null,
        val virtual: Boolean = false
) : Module(parent, name) {

    companion object {
        private val log = logger(Level.ALL)

        const val CORE_FREQ = 8_000_000
        const val OVER8 = 0
    }

    inner class Ports : ModulePorts(this) {
        val mem = Slave("mem", 0x400)
        val irq = Master("irq", PIN)
    }

    override val ports = Ports()

    open inner class USART_REGISTER(port: SlavePort, address: Long, name: String, default: Long, writeable: Boolean = true) :
            Register(port, address, DWORD, name, default, writeable) {
        override fun read(ea: Long, ss: Int, size: Int): Long {
            log.info { "[${core.pc.hex8}] RD $this <- ${data.hex8}" }
            return super.read(ea, ss, size)
        }

        override fun write(ea: Long, ss: Int, size: Int, value: Long) {
            log.info { "[${core.pc.hex8}] WR $this -> ${data.hex8} to ${value.hex8}" }
            super.write(ea, ss, size, value)
        }
    }

    inner class USART_CR1_REG : USART_REGISTER(ports.mem, 0x00, "USART_CR1", 0x0000_0000) {
        var TE by bit(3)
        var RE by bit(2)
        var UE by bit(0)

        override fun write(ea: Long, ss: Int, size: Int, value: Long) {
            super.write(ea, ss, size, value)

            if (!comEnabled && UE == 1) {
                if (TE == 1) transmitEnabled = true
                if (RE == 1) receiveEnabled = true

                // transmitEnabled и receiveEnabled должен быть в true до вызова openComPort!
                if (UE == 1) {
                    log.warning { "USART$index enabled!" }
                    // TODO: Необходимо реализовать правильный parity и остальные параметры
                    comPort = openComPort(comName, baudRate)
                }

                if (!comEnabled) {
                    transmitEnabled = false
                    receiveEnabled = false
                    log.severe { "USART$index can't enable usart something goes wrong..." }
                } else {
                    if (transmitEnabled) {
                        log.warning { "USART$index transmit enabled" }
                        USART_ISR.TEACK = 1
                    }

                    if (receiveEnabled) {
                        log.warning { "USART$index receive enabled" }
                        USART_ISR.REACK = 1
                    }
                }
            } else if (comEnabled && UE == 0) {
                TODO("UART disable not implemented!")
            }
        }
    }

    inner class USART_ISR_REG : USART_REGISTER(ports.mem, 0x1C, "USART_ISR", 0x0200_00C0, false) {
        var REACK by bit(22)
        var TEACK by bit(21)
        var TXE by bit(7)
        var TC by bit(6)
        var RXNE by bit(5)

        // TODO: Реализовать флаги TXE RXNE TC
        override fun read(ea: Long, ss: Int, size: Int): Long {
            RXNE = if (queueRx.size != 0) 1 else 0

            if (queueTx.remainingCapacity() != 0) {
                TXE = 1
                TC = 1
            } else {
                TXE = 0
                TC = 0
            }

            return super.read(ea, ss, size)
        }
    }

    inner class USART_BRR_REG : USART_REGISTER(ports.mem, 0x0C, "USART_BRR", 0x0000_0000) {
        var BRR by field(15..0)

        override fun write(ea: Long, ss: Int, size: Int, value: Long) {
            super.write(ea, ss, size, value)
            when (OVER8) {
                0 -> {
                    if (USART_CR1.UE != 0) {
                        log.severe { "USART$index not disabled but BRR written -> this operation has not effect!" }
                        return
                    }

                    val USARTDIV = BRR
                    baudRate = calcBaudRate(CORE_FREQ, USARTDIV)

                    log.warning { "USART$index baud rate set to $baudRate baud" }
                }
                else -> throw NotImplementedError("OVER8 = 1 not implemented!")
            }
        }
    }

    inner class USART_RDR_REG : USART_REGISTER(ports.mem, 0x24, "USART_RDR", 0x0000_0000, false) {
        var RDR by field(7..0)

        override fun read(ea: Long, ss: Int, size: Int): Long {
            // Небольшой gap на всякий случай, должен быть не null, так как читать этот регистр можно
            // только если выставлен флаг в ISR-регистре, что в RDR что-то есть
            val byte = queueRx.poll(10, TimeUnit.MILLISECONDS)
            if (byte != null) {
                RDR = byte.asUInt
                log.warning { "USART$index receive ${RDR.hex2}/${RDR.toChar()}" }
            } else {
                log.severe { "USART$index reading empty UART -> something wrong with flags..." }
            }
            return super.read(ea, ss, size)
        }
    }

    inner class USART_TDR_REG : USART_REGISTER(ports.mem, 0x28, "USART_TDR", 0x0000_0000) {
        var TDR by field(7..0)

        override fun write(ea: Long, ss: Int, size: Int, value: Long) {
            super.write(ea, ss, size, value)
            // Небольшой gap на всякий случай, offer должен вернуть true, так как писать этот регистр можно
            // только если выставлен флаг в ISR-регистре, что в TDR передача завершена
            if (queueTx.offer(TDR.asByte, 10, TimeUnit.MILLISECONDS)) {
                log.warning { "USART$index transmit byte ${TDR.hex2}/${TDR.toChar()}" }
                queueTx.put(TDR.asByte)
            } else {
                log.severe { "USART$index writing full UART -> something wrong with flags..." }
            }
        }
    }

    private val USART_CR1 = USART_CR1_REG()
    private val USART_CR2 = USART_REGISTER(ports.mem, 0x04, "USART_CR2", 0x0000_0000)
    private val USART_CR3 = USART_REGISTER(ports.mem, 0x08, "USART_CR3", 0x0000_0000)
    private val USART_BRR = USART_BRR_REG()
    private val USART_GTPR = USART_REGISTER(ports.mem, 0x10, "USART_GTPR", 0x0000_0000)
    private val USART_RTOR = USART_REGISTER(ports.mem, 0x14, "USART_RTOR", 0x0000_0000)
    private val USART_RQR = USART_REGISTER(ports.mem, 0x18, "USART_RQR", 0x0000_0000)
    private val USART_ISR = USART_ISR_REG()
    private val USART_ICR = USART_REGISTER(ports.mem, 0x20, "USART_ICR", 0x0000_0000)
    private val USART_RDR = USART_RDR_REG()
    private val USART_TDR = USART_TDR_REG()

    private fun calcBaudRate(clock: Int, div: Int): Int = clock / div + 1

    private var comPort: SerialPort? = null

    private val comEnabled get() = comPort != null
    private var transmitEnabled = false
    private var receiveEnabled = false

    private var baudRate = -1

    private val queueTx = LinkedBlockingQueue<Byte>(128)
    private val queueRx = LinkedBlockingQueue<Byte>(128)

    /**
     * Функция пытается открыть реальный ком-порт,
     * если имя не задано или если произошла ошибка при открытии,
     * то будет возвращен null и порт фактически не будет работать.
     *
     * @param name имя реального com-порта
     * @param baudRate реальная скорость порта
     * @param parity четность реального порта
     * @param numStopBits количество стоп-бит реального порта
     * @param numDataBits количество дата-бит реального порта
     *
     * @return null, если при открытии порта произошла ошибка, иначе объект класса [SerialPort]
     */
    private fun openComPort(
            name: String?,
            baudRate: Int,
            parity: Int = SerialPort.NO_PARITY,
            numStopBits: Int = SerialPort.ONE_STOP_BIT,
            numDataBits: Int = 8): SerialPort? {

        if (virtual)
            throw NotImplementedError("USART$index -> Automatic creation of virtual port not implemented!")

        if (name == null) {
            log.severe { "USART$index -> Trying open real com-port when name was not specified!" }
            return null
        }

        val comPort = SerialPort.getCommPort(name)
        comPort.baudRate = baudRate
        comPort.parity = parity
        comPort.numStopBits = numStopBits
        comPort.numDataBits = numDataBits

        if (!comPort.openPort()) {
            log.severe { "USART$index -> Can't open port: $name" }
            return null
        }

        val listener = object : SerialPortDataListener {
            override fun getListeningEvents() = SerialPort.LISTENING_EVENT_DATA_AVAILABLE

            override fun serialEvent(event: SerialPortEvent) {
                if (event.eventType != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return
                val newData = ByteArray(comPort.bytesAvailable())
                val numRead = comPort.readBytes(newData, newData.size.toLong())
                log.info { "Received $numRead bytes from $name [${newData.hexlify()}]" }
                newData.forEach { queueRx.put(it) }
            }
        }

        val thread = thread {
            while (transmitEnabled) {
                // TODO: Необходимо добавить проверку наличия байта в очереде без вечного блока
                val byte = queueTx.poll(100, TimeUnit.MILLISECONDS)

                if (byte != null)
                    comPort.outputStream.write(byte.asUInt)
            }
        }

        if (receiveEnabled)
            comPort.addDataListener(listener)

        return comPort
    }

    override fun reset() {
        super.reset()
        //TODO("Не реализованно, должно быть освобождение реального ком-порта!")
    }
}