package ru.inforion.lab403.kopycat.modules.stm32f042

import ru.inforion.lab403.common.logkot.logger
import ru.inforion.lab403.common.misc.get
import ru.inforion.lab403.common.misc.insert
import ru.inforion.lab403.kopycat.cores.base.common.Module
import ru.inforion.lab403.kopycat.cores.base.common.ModulePorts
import ru.inforion.lab403.kopycat.cores.base.enums.Datatype.DWORD
import ru.inforion.lab403.kopycat.modules.PIN
import java.util.logging.Level

/**
 * Created by user on 13.07.17.
 */
class RCC(parent: Module, name: String) : Module(parent, name) {
    companion object { private val log = logger(Level.ALL) }
    inner class Ports : ModulePorts(this) {
        val mem = Slave("mem")
        val irq = Master("irq", PIN)
    }
    override val ports = Ports()

    private inner class RCC_CFGR_TYP : Register(ports.mem, 0x04, DWORD, "RCC_CFGR", 0x0000_0000) {
        override fun write(ea: Long, ss: Int, size: Int, value: Long) = super.write(ea, ss, size, value.insert(value[1..0], 3..2))
    }

    private val RCC_CR      = Register(ports.mem, 0x00, DWORD, "RCC_CR",      0x0000_0083)
    private var RCC_CFGR    = RCC_CFGR_TYP()
    private val RCC_CIR     = Register(ports.mem, 0x08, DWORD, "RCC_CIR",     0x0000_0000)
    private val RCC_AHBENR  = Register(ports.mem, 0x14, DWORD, "RCC_AHBENR",  0x0000_0014)
    private val RCC_APB2ENR = Register(ports.mem, 0x18, DWORD, "RCC_APB2ENR", 0x0000_0000)
    private val RCC_APB1ENR = Register(ports.mem, 0x1C, DWORD, "RCC_APB1ENR", 0x0000_0000)
    private val RCC_CFGR2   = Register(ports.mem, 0x2C, DWORD, "RCC_CFGR2",   0x0000_0000)
    private val RCC_CFGR3   = Register(ports.mem, 0x30, DWORD, "RCC_CFGR3",   0x0000_0000)
    private val RCC_CR2     = Register(ports.mem, 0x34, DWORD, "RCC_CR2",     0x0000_0080)
}